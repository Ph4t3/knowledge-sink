---
aliases: [Data Structures & Algorithms]
---
# Data Structures & Algorithms
Collection of short descriptions and code snippets of various data structures and algorithms. Prepared for various placement and job interviews. 
 
```ccard
type: folder_brief_live
noteOnly: true
```

- [[Sorting]]
- [[Linked Lists]]
- [[Hash Tables]]

### TODO
- [[Binary Tree (TODO)]]
- [[Binary Search Tree (TODO)]]
- [[Heap (TODO)]] 
- [[Graph (TODO)]]
- [[Priority Queue (TODO)]]