# Tortoise & Hare
> Also called as #Floyd's Cycle Detection

- Use a fast ptr and a slow ptr
- Increment fast ptr by 2
- When fast reaches 2nd, slow will be in the middle

## Use Cases
- Delete middle element of linked list
- Merge sort Linked list (find middle element)
- Find cycle in linked list
	- Happens when both Tortoise and Hare meet