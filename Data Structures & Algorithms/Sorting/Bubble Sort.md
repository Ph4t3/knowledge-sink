# Bubble Sort
Bubble Sort works by repeatedly swapping the adjacent elements if they are in wrong order.

1. Iterate through the array
2. For each element, check if it's greater/smaller than next element and swap if needed
3. At end of the loop, greatest/smallest element will be at the end of the array
4. Loop again but without considering the last element
5. Iterate until single element remains

```C++
void bubbleSort(vector<int>& arr)
{
    for (auto i = arr.size() - 1; i != 0; i--) {
        for (auto j = 0; j != i; j++) {
            if (arr[j] > arr[j + 1]) {
                swap(arr[j], arr[j + 1]);
            }
        }
    }
}
```