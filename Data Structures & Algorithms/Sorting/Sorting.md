---
aliases: [Sorting]
---
# Sorting

A list of various sorting algorithms
 
```ccard
type: folder_brief_live
noteOnly: true
style: 
```

- [[Bubble Sort]]
- [[Selection Sort]]
- [[Insertion Sort]]
- [[Merge Sort]]
- [[Quick Sort]]
- [[Heap Sort]]