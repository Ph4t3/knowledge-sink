# Merge Sort
Merge sort is a #DivideAndConquer algorithm. Split the array into 2 equal halves, sort them (recursively call merge sort), and merge the two sorted arrays.
> Array with one element is already sorted :)

```C++
void merge(vector<int>& arr, int start, int mid, int end)
{
    vector<int> left, right;
    left.assign(arr.begin() + start, arr.begin() + mid + 1);
    right.assign(arr.begin() + mid + 1, arr.begin() + end + 1);

    int indexOfLeft = 0, indexOfRight = 0, indexOfMerged = start;
    while (indexOfLeft < left.size() && indexOfRight < right.size()) {
        if (left[indexOfLeft] < right[indexOfRight])
            arr[indexOfMerged++] = left[indexOfLeft++];
        else
            arr[indexOfMerged++] = right[indexOfRight++];
    }

    while (indexOfLeft < left.size()) {
        arr[indexOfMerged++] = left[indexOfLeft++];
    }

    while (indexOfRight < right.size()) {
        arr[indexOfMerged++] = right[indexOfRight++];
    }
}

void mergeSort(vector<int>& arr, int start, int end)
{
    if (start >= end)
        return;

    int mid = (start + end) / 2;
    mergeSort(arr, start, mid);
    mergeSort(arr, mid + 1, end);
    merge(arr, start, mid, end);
}
```