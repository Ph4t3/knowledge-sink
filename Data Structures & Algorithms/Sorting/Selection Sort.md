# Selection Sort
Selection sort selects the element one by one and keeps them in their right place.

1. Iterate and find the smallest/largest element
2. Swap with the first element in the array
3. Loop again by excluding the first element
4. Do until the sub array only has one element

```C++
void selectionSort(vector<int>& arr)
{
    for (int i = 0; i < arr.size() - 1; i++) {
        int min = arr[i];
        for (int j = i + 1; j < arr.size(); j++) {
            if (arr[j] < min)
                swap(arr[j], min);
        }
        swap(min, arr[i]);
    }
}
```