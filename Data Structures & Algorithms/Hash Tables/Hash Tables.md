# Hash Tables
```ccard
type: folder_brief_live
noteOnly: true
```

Hashing is a technique or process of mapping keys, values into the hash table by using a hash function. It is done for faster access to elements. The efficiency of mapping depends on the efficiency of the hash function used.

## Hash Functions

>##### Hash Function
>Used to compute a hash value from a key *k*. 

### Division method
> h(k) = k mod m
### Multiplication method
> h(k) = ⌋ m(kA mod 1) ⌊
- Multiply key by a constant A
- Take the fractional part 
- Multiply by length of hash table (m)
- Take floor of it

## Collision Resolution

>##### Collision
>When two keys hash to the same value, its called *collision*.

### Chaining
Place all elements that hashed to the same value into a linked list.
- *Insert* = O(1)
	- Insert at the head of linked list
- *Search* = ϴ(1 + α)
	- α = n/m
		- n = \#total_elements
		- m = \#elements in hash table

### Open Addressing
All elements occupy the hash table itself. Therefore at any point of time \#elements <= size of hash table

To perform insertion using open addressing, we successively examine, or **probe**, the hash table until we ﬁnd an empty slot in which to put the key. Instead of being ﬁxed in the order 0, 1, . . . , m - 1 (which requires ϴ(n) search time), the sequence of positions probed depends upon the key being inserted.

With open addressing, we require that for every key k, the **probe sequence**
><h(k,0), h(k,1), . . . , h(k, m-1)>

be a permutation of <0, 1, . . . , m - 1>

#### Operations

##### Insertion
Go through the probe sequence and until an empty slot is found. Insert it there.

##### Search
Go through the probe sequence until element is found or an empty slot is reached.

##### Deletion
Instead of making the slot NIL, mark it as deleted. So that *search* will continue instead of halting.

#### Uniform Hashing
The **probe sequence** of each key
is equally likely to be any of the *m!* permutations of <0, 1, . . . , m-1>

True uniform hashing is difﬁcult to implement, however, and in practice suitable approximations are used. They are defined below.

##### Linear Probing

>h(k, i) = (h'(k) + i) mod m

##### Quadratic Probing

>h(k, i) = ( h'(k) + c<sub>1</sub> *i* + c<sub>2</sub> *i*<sup>2</sup> ) mod m

##### Double Hashing

> h(k, i) = ( h<sub>1</sub>(k) + *i* h<sub>2</sub>(k)) mod m

The value h<sub>2</sub>(k) must be relatively prime to the hash-table size m for the entire hash table to be searched.