|     | 8:00 | 9:00 | 10:15 | 11:15 | 1:00 | 2:00 | 5:00 |
| --- | ---- | ---- | ----- | ----- | ---- | ---- | ---- |
| Mon |      | QC   | CV    | AI    |      |      | TC   |
| Tue | AI   |      |       | ML    |      | QC+  | TC   |
| Wed | ML   |      | QC    | CV    | TC   |      |      |
| Thu | CV   | AI   |       |       |      | ML+  |      |
| Fri |      | ML   |       | QC    | TC+  | AI+  | CV+  | 

- 2:00 - 5:00 Thursday : Project 1

- [ ] Quantum Computation - F
- [ ] Artificial Intelligence - B
- [ ] Topics in Cryptography - H
- [ ] Machine Learning - C
- [ ] Computer Vision - D
- [ ] Project Part 1 - S